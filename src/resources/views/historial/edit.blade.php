@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="page-header">
                    <br>
                    <hr>
                    <h2>Editar Entrada de Stock</h2>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Editar Información</div>
                    <div class="card-body">
                        <form action="{{ route('historial.update', $movimiento->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="producto">Producto:</label>
                                <input type="text" class="form-control" id="producto" name="producto" value="{{ $movimiento->producto->nombre }} {{ $movimiento->producto->marca }}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="cantidad">Cantidad:</label>
                                <input type="number" class="form-control" id="cantidad" name="cantidad" value="{{ $movimiento->cantidad }}" required>
                            </div>
                            <div class="form-group">
                                <label for="precio">Precio:</label>
                                <input type="text" class="form-control" id="precio" name="precio" value="{{ $movimiento->precio}}" required>
                            </div>
                            <div class="form-group">
                                <label for="fecha">Fecha:</label>
                                <input type="text" class="form-control" id="fecha" name="fecha" value="{{ \Carbon\Carbon::parse($movimiento->fecha)->format('d/m/Y H:i:s') }}" readonly>
                            </div>
                            <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                            <a href="{{ route('historial.entradas') }}" class="btn btn-secondary">Cancelar</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
